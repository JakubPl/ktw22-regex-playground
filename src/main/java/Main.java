import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        String phoneNumber = "796 333-333";
        System.out.println("Czy to jest numer telefonu?");
        System.out.println(phoneNumber.matches("\\d{3}[- ]?\\d{3}[- ]?\\d{3}"));
        System.out.println("Pierwsze 3 cyfry numeru za pomoca split");
        System.out.println(phoneNumber.split("[- ]")[0]);
        System.out.println("Pierwsze 3 cyfry numeru za pomoca Pattern");
        Pattern phonePattern = Pattern.compile("(\\d{3})[- ]?(\\d{3})[- ]?\\d{3}");
        Matcher matcher = phonePattern.matcher(phoneNumber);
        if(matcher.find()) {
            System.out.println(matcher.group(1) + matcher.group(2));
        } else {
            System.out.println("To nie numer telefonu");
        }
        System.out.println("Pierwsze 3 cyfry numeru za pomoca replaceAll");
        System.out.println(phoneNumber.replaceAll("(\\d{3})[- ]?(\\d{3})[- ]?\\d{3}", "$1abcd$2"));

        System.out.println("Czy to jest mail w onet.pl albo gmail.com");
        String maybeMail = "jacek_placek@gmail.com";

        Pattern mailPattern = Pattern.compile("\\w+@(onet\\.pl|gmail\\.com)");
        Matcher mailMatcher = mailPattern.matcher(maybeMail);
        if(mailMatcher.find()) {
            System.out.println("Znaleziono!");
            System.out.println(mailMatcher.group(1));
        } else {
            System.out.println("Nie znaleziono!");
        }


    }
}
